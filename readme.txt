=== Sofia ===

Contributors: PGeorgiev
Tags: custom-background, custom-logo, custom-menu

Requires at least: 4.5
Tested up to: 4.8
Stable tag: 1.4
License: GNU General Public License v2 or later
License URI: LICENSE

A starter theme called Serdika.

== Copyright ==
Theme Name: Serdika
Theme URI: https://themes.atomicthemes.org/serdika;
Author: Plamen Georgiev
Author URI: http://pgeorgiev.it/
Description: Serdika - Minimal blog theme

== Description ==

Serdika - Minimal blog theme

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Sofia includes support for Infinite Scroll in Jetpack.

== Changelog ==


= 1 August 2018 =
* Initial release

= 1 August 2018 =
* Added nav style
== Credits ==

= 16 September 2018 =
* Fixed nav menu four tiers
* Fixed footer name serdika to Serdika (Capital S)
* Fixed AGAIN footer.php

= 5 November 2018 =
* Edited function topcat_lite_social_menu() (topcat - topcategory). Now: serdika_social_menu
* Theme URI working corectly
* Deleted blank README.MD
* Added Google Font Playfair Display to functions.php / Removed from style.css /


